package common

import (
	"layim/controllers"
)

func Router() { //Router 设置全局router
	chat := controllers.ChatController{Orm: GetOrm()}
	desktop := controllers.DesktopController{Orm:GetOrm()}
	app.Any("/upload", chat.Upload)
	im := app.Party("/imapi")
	im.Any("/friends", chat.Friends)
	im.Any("/chatlog", chat.ChatLog)
	im.Post("/login", chat.Login)
	im.Any("/register", chat.Register)
	im.Get("/logout", chat.Logout)
	im.Post("/sign", chat.Sign)
	im.Get("/group", chat.Group)
	im.Post("/changeStatus", chat.Change)
	im.Get("/find", chat.Find)
	im.Post("/ajaxFind", chat.AjaxFind)
	im.Get("/msgbox", chat.Msgbox)  //展示
	im.Post("/delete", chat.Delete) //删除群组 聊天群 分组
	im.Post("/addFind", chat.AddFriend)
	im.Post("/addFindGroup", chat.AddRoom)
	im.Any("/editinfo", chat.EditInfo)


	app.Get("/", desktop.Index).ChangeName("home")
	app.Any("/sendmail", desktop.SendMail)
	app.Get("/verify-email", desktop.VerifyMail)
	app.Get("/calendar", desktop.Calendar)
	app.Any("/cropbox", desktop.CropBox)
	app.Get("/technical-support",desktop.TechnicalSupport)
	app.Get("/setting",desktop.Setting)
	app.Get("/theme",desktop.Theme)
}
