package main

import (
	"layim/common"
)

func main() {
	//设置静态资源
	common.SetIrisStatic()
	//设置站点配置
	common.SetIrisConfig()
	//设置中间件
	common.Middleware()
	//获取全局ORM
	common.GetOrm()
	//设置错误页面显示
	common.SetErrorPage()
	//设置路由
	common.Router()
	//更新多余数据
	common.InitClearUserWsId()
	//设置websocket
	common.SetWebSocket("/imserver")
	//运行服务器
	common.RunServer()
}
