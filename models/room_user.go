package models

type RoomUser struct {
	Id     int64
	UserId int64
	RoomId int64
}
