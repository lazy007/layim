package models

type Calendar struct {
	Id      int64	`json:"id"`
	Title   string	`json:"title"`
	Content string	`json:"content"`
	Start   string	`json:"start"`
	Uid     int64	`json:"uid"`
}
