package models

type Room struct {
	Id     int64
	Name   string
	Avatar string
	UserId int64
}
