package models

type User struct {
	Id       int64 `gorm:"primary_key"`
	Username string
	Status   int64
	Sign     string
	Avatar   string
	Email    string
	Password string
	Wsid     string
	Token	string
}
