package models

type Chatlog struct {
	Id       int64 `gorm:"primary_key"`
	UserId   int64
	FriendId int64
	Message  string
	Time     int64
}
