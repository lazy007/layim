package models

//用户好友分组
type Group struct {
	Id     int64  `gorm:"primary_key"`
	UserId int64
	Name   string
}
