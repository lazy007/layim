package models

type Friend struct {
	Id       int64  `gorm:"primary_key"`
	UserId   int64
	FriendId int64
	NickName string
	GroupId  int64
}
