layui.use(['jquery', 'layer', "form"], function ($, layer, from) {
    from();
    var btngroup = $(".find-btn-group .layui-btn");
    btngroup.click(function () {
        if ($(this).hasClass("layui-btn-normal")) {
            return false;
        }
        btngroup.removeClass("layui-btn-normal").removeClass("layui-btn-primary");
        var index = $(this).index();
        btngroup.eq(index).addClass("layui-btn-normal");
        btngroup.eq(index ? 0 : 1).addClass("layui-btn-primary");
        $("[name='type']").val(index ? "group" : "friend");
        $(".find-tips").html(index ? "通过群号查找群组" : "通过账号查找联系人");
        $("[name='findnum']").val('');
    });
    $(".getNext").click(function () {
        var findnum = $("[name='findnum']").val();
        var type = $("[name='type']").val();
        if (!/^[1-9]\d{4,8}$/.test(findnum)) {
            parent.layer.alert("请填写查找的联系人账号/群号");
            return
        }
        $.post(parent.API.ajaxFind, {"number": findnum, "type": type}, function (data) {
            if (data.errcode) {
                parent.layer.alert(data.message);
            } else {
                if (data.message == "friend") {
                    $(".find-info,.prev,.addFriend").show();
                    $(".find-box,.getNext,.addGroup").hide();
                    $(".find-info-avatar").attr("src", data.data.avatar);
                    $(".find-info-username").html(data.data.username);
                    $(".find-info-group").show();
                } else {
                    $(".find-info,.prev,.addGroup").show();
                    $(".find-box,.getNext.addFriend").hide();
                    $(".find-info-avatar").attr("src", data.data.avatar);
                    $(".find-info-username").html(data.data.groupname);
                    $(".find-info-group").hide();
                }
            }
        }, 'json');
        return false;
    });
    $(".prev").click(function () {
        $(".find-info,.prev,.addFriend,.addGroup").hide();
        $(".find-box,.getNext").show();
    });
    $(".addFriend").click(function () {
        var addfriend = $("#groupid").val();
        if (addfriend === "") {
            parent.layer.alert("请选择要加入的分组");
            return false;
        }
        var num = $("[name='findnum']").val();
        $.post(parent.API.addfriend, {friendid: num, groupid: addfriend}, function (data) {
            parent.layer.alert(data.message);
            //todo 调用父页面往socketServer发送添加好友数据
            if (data.errcode == 0) {
                parent.layui.layim.addList(data.data);
                parent.socket.send(JSON.stringify({  //给对应的用户推送数据
                    type: parent.SEVENT.ADDFRIEND,
                    from: parent.API.userid,
                    to:parent.num
                }));
            }
        }, 'json');
    });

    $(".addGroup").click(function () {
        var num = $("[name='findnum']").val();
        $.post(parent.API.addroom, {roomid: num}, function (data) {
            parent.layer.alert(data.message);
            if (data.errcode == 0) {
                parent.layui.layim.addList(data.data);
            }
        }, 'json');
    });
});