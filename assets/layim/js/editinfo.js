layui.use(['layer', "jquery", 'form', 'upload'], function (layer, $, form) {
    form = form();
    $("#cropImg").click(function () {
        var imgurl = $("#L_avatar").val();
        var cropImg = layui.layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ['650px', '535px'],
            content: [top.API.cropbox + '?imgurl=' + imgurl, 'no'],
            btn: ['取消裁剪'],
            yes: function () {
                layui.layer.close(cropImg);
            }
        });
    });
    layui.upload({
        url: top.API.upload + '?type=avatar&field=avatar'
        , ext: 'jpg|png|jpeg' //那么，就只会支持这三种格式的上传。注意是用|分割。
        , before: function (input) {
            console.log('文件上传中');
        }
        , success: function (res) {
            console.log(res)
            if (res.errcode) {
                layer.alert(res.message);
            } else {
                $(".dkuang1").css("background-image", "url(" + res.data + ")");
                $("#L_avatar").val(res.data);
            }
        }
    });
    form.verify({
        username: function (value) {
            if (value.length < 2 || value.length > 16) {
                return '账号长度为2~16个字符！';
            }
        }
    });
    form.on('submit(tj)', function (data) {
        layer.confirm('确认无误，提交修改？', {
            btn: ['确定', '取消'], //按钮
            icon: 1,
            title: '信息修改'
        }, function () {
            layer.closeAll();
            var username = $("#L_username").val();
            var avatar = $("#L_avatar").val();
            var sign = $("#L_sign").val();
            $.post(parent.API.editinfo,
                {username: username, avatar: avatar, sign: sign},
                function (data) {
                    layer.msg(data.message);
                    if (data.errcode == 0) {
                        parent.layui.jquery(".layui-layim-user").html(username);
                        parent.layui.jquery(".layui-layim-remark").val(sign);
                        setTimeout(function () {
                            top.layui.layer.closeAll('iframe');
                        }, 1000);
                    }
                }, 'json');
        });
        return false;
    });
});




