iziToast.settings({
    timeout: 50000,
    pauseOnHover: true,
    zindex: 9999999999,
    close: true,
    progressBar: true,
    balloon: true,
    transitionIn: 'flipInX',
    transitionOut: 'flipOutX'
});
if (typeof flash.result != "undefined") {
    if (flash.result) {
        iziToast.success({
            title: '成功',
            message: flash.message
        });
    } else {
        iziToast.error({
            title: '失败',
            message: flash.message
        });
    }
}
layui.use(['layim', 'layer', "menu", "upload", "form"], function () {
    var layim = layui.layim,
        layer = layui.layer,
        $ = layui.jquery,
        menu = layui.menu,
        upload = layui.upload;
    if (USERID) {
        API.userid = USERID;
        desktop(layui);
        IM(layim, socket = new WebSocket(HOST + API.server), $, menu);
        return false;
    }
    var loginWin = layer.open({
        type: 1,
        title: "IrisGo 轻聊版",
        closeBtn: 1,
        area: ['420px', '240px'], //宽高
        content: '<form class="layui-form layui-form-pane login-win" onsubmit="return false">\
            <div class="layui-form-item">\
            <label class="layui-form-label">账号</label>\
            <div class="layui-input-block">\
            <input type="text" id="username" placeholder="请输入账号" class="layui-input layui-form-danger">\
            </div>\
            </div>\
            <div class="layui-form-item">\
            <label class="layui-form-label">密码</label>\
            <div class="layui-input-block">\
            <input type="password" id="password" placeholder="请输入密码" class="layui-input layui-form-danger" >\
            </div>\
            </div>\
            <div class="layui-form-item login-btnbox">\
            <button class="layui-btn login-btn">登陆</button>\
            <button class="layui-btn reg-btn layui-btn-primary">注册</button>\
            </div></form>'
    });
    $(".login-btn,.reg-btn").click(function () {
        if (socket != null) return false;
        var url = API.login;
        if ($(this).hasClass("reg-btn")) {
            layer.closeAll();
            return layer.open({
                type: 2,
                title: '用户注册',
                area: ['904px', '443px'],
                fixed: true,
                content: API.register,
                end: function () {
                    location.reload();
                }
            });
        }
        var username = $("#username").val();
        var password = $("#password").val();
        if (!username || !password) {
            layer.msg('用户名或密码不能为空');
            return false;
        }
        var sendData = {username: username, password: password};
        //加载层-风格4
        var loginLoading = layer.msg('登录中...', {
            icon: 16
            , shade: 0.01
        });
        $.post(url, sendData, function (data) {
            if (data.errcode != 0) {
                if (typeof data.data == "string") {
                    var content = '<div style="text-align: center;margin-top: 20px;font-size: 14px;">' + data.message + '&nbsp;<a style="font-size: 14px;" class="layui-btn" href="###" onclick="send(this,layui.jquery,\'' + data.data + '\');">验证</a></div>';
                    layer.open({
                        type: 1,
                        title: "IrisGo 轻聊版",
                        closeBtn: 1,
                        content: content,
                        area: ['420px', '240px']
                    });
                } else {
                    layer.msg(data.message);
                }
            } else {
                layer.close(loginWin);  //关闭登陆窗口
                layer.close(loginLoading);
                layer.msg('登陆成功,数据加载中...', {
                    icon: 16
                    , shade: 0.01
                });
                API.userid = data.data.userid;
                desktop(layui);
                IM(layim, socket = new WebSocket(HOST + API.server), $);
            }
        }, API.xhrtype);
    });
});
function send(obj, $, url) {
    var self = $(obj);
    if (!self.hasClass("layui-btn-disabled")) {
        $.get(url, "", function (data) {
            var timer = null, i = 60;
            if (data.errcode) {
                layer.msg(data.message);
            } else {
                self.html("已发送(" + i + ")").addClass("layui-btn-disabled");
                timer = setInterval(function () {
                    if (i == 1) {
                        clearInterval(timer);
                        self.html("发送").removeClass("layui-btn-disabled");
                    } else {
                        i--;
                        self.html("已发送(" + i + ")");
                    }
                }, 1000);
            }
        });
    }
}