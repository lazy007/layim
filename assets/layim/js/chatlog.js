layui.use(['layim','laytpl','jquery'], function (layim,laytpl,$) {
    //聊天记录的分页此处不做演示，你可以采用laypage，不了解的同学见文档：http://www.layui.com/doc/modules/laypage.html
    //开始请求聊天记录
    var param = location.search //获得URL参数。该窗口url会携带会话id和type，他们是你请求聊天记录的重要凭据
        //实际使用时，下述的res一般是通过Ajax获得，而此处仅仅只是演示数据格式
        , res = {
                code: 0
                , msg: ''
                , data: data
}
    var html = laytpl($("#listtpl").val()).render({
        data: res.data
    });
    $('#LAY_view').html(html);
});