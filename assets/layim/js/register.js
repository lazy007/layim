function send(obj,$,url) {
    var self = $(obj);
    if (!self.hasClass("layui-btn-disabled")) {
        $.get(url,"", function (data) {
            var timer = null, i = 60;
            if (data.errcode) {
                layer.msg(data.message);
            } else {
                self.html("已发送("+i+")").addClass("layui-btn-disabled");
                timer = setInterval(function () {
                    if (i == 1) {
                        clearInterval(timer);
                        self.html("发送").removeClass("layui-btn-disabled");
                    } else {
                        i--;
                        self.html("已发送(" + i + ")");
                    }
                }, 1000);
            }
        });
    }
}
layui.use(['layer', "jquery", 'form', 'upload'], function (layer, $, form) {
    form = form();
    $("#cropImg").click(function () {
        var imgurl = $("#L_avatar").val();
        if (imgurl.indexOf("layim") > -1) return false;
        var cropImg = layui.layer.open({
            type: 2,
            title: false,
            closeBtn: 0,
            area: ['650px', '535px'],
            content: [top.API.cropbox + '?imgurl=' + imgurl, 'no'],
            btn: ['取消裁剪'],
            yes: function () {
                layui.layer.close(cropImg);
            }
        });
    });
    layui.upload({
        url: parent.API.upload + '?type=avatar&field=avatar'
        , ext: 'jpg|png|jpeg' //那么，就只会支持这三种格式的上传。注意是用|分割。
        , before: function (input) {
            console.log('文件上传中');
        }
        , success: function (res) {
            console.log(res)
            if (res.errcode) {
                layer.alert(res.message);
            } else {
                $(".dkuang1").css("background-image", "url(" + res.data + ")");
                $("#L_avatar").val(res.data);
            }
        }
    });
    form.verify({
        username: function (value) {
            if (value.length < 2 || value.length > 16) {
                return '账号长度为2~16个字符！';
            }
        },
        password: function (value) {
            if (value.length < 6 || value.length > 16) {
                return '密码长度为6~16个字符！';
            }
        },
        required: function (value) {
            if (value != document.getElementById("L_pass").value) {
                return '确认密码有误！';
            }
        }
    });
    form.on('submit(tj)', function (data) {
        layer.confirm('确认无误，提交注册？', {
            btn: ['确定', '取消'], //按钮
            icon: 1,
            title: '用户注册'
        }, function () {
            layer.closeAll();
            var email = $("#L_email").val();
            var username = $("#L_username").val();
            var password = $("#L_pass").val();
            var avatar = $("#L_avatar").val();
            $.post(parent.API.register, {
                email: email,
                username: username,
                password: password,
                avatar: avatar
            }, function (data) {
                var content = '<div style="text-align: center;margin-top: 150px;font-size: 22px;">' + data.message + ',如未收到请再次&nbsp;&nbsp;&nbsp;<a style="font-size: 14px;" class="layui-btn" href="###" onclick="send(this,layui.jquery,\''+data.data+'\');">发送</a></div>';
                if (data.errcode == 0) {
                    layer.open({
                        type: 1,
                        title: false,
                        closeBtn: 0,
                        content: content,
                        area: ['100%', '100%']
                    });
                } else {
                    layer.msg(data.message);
                }
            }, 'json');
        });
        return false;
    });
});


