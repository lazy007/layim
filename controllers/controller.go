package controllers

import (
	"github.com/kataras/go-serializer"
	"fmt"
	"github.com/kataras/go-serializer/json"
)



//responseJSON Http类型请求返回结果API
type responseJSON struct {
	Code    int         `json:"code"`
	Errcode int         `json:"errcode"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

//websocketJSON 的借口返回数据内容
type websocketJSON struct {
	From    int64       `json:"from"`
	To      int64       `json:"to"`
	Message interface{} `json:"message"`
	Type    string      `json:"type"`
}

//FrindUser结构
type UserInfo struct {
	Id     int64  `json:"id"`
	Name   string `json:"username"`
	Status string `json:"status"`
	Sign   string `json:"sign"`
	Avatar string `json:"avatar"`
}

//ChatLog 结构
type ChatList struct {
	Id      int64  `json:"id"`
	Name    string `json:"username"`
	Avatar  string `json:"avatar"`
	Time    string `json:"time"`
	Content string `json:"content"`
}

//FriendGroup结构
type GroupList struct {
	Id   int64      `json:"id"`
	Name string     `json:"groupname"`
	List []UserInfo `json:"list"`
}

//聊天室分组
type GroupInfo struct {
	Id     int64  `json:"id"`
	Name   string `json:"groupname"`
	Avatar string `json:"avatar"`
}

//friendAPI接口返回数据结构
type FriendAPIJSON struct {
	Code int64       `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

//事件类型定义
type EventType struct {
	LOGIN        string //登陆
	LOGOUT       string //退出
	MESSAGE      string //消息
	CHANGESTATUS string //改变状态通知
	CHATROOM     string //聊天室
	ADDFRIEND    string //添加朋友
}

type FlashSession struct {
	Result  bool        `json:"result"`
	Message string        `json:"message"`
}

//事件类型定义
var WSEventType EventType = EventType{
	LOGIN:        "login",
	LOGOUT:       "logout",
	MESSAGE:      "message",
	CHANGESTATUS: "change",
	CHATROOM:     "chatroom",
	ADDFRIEND:    "addfriend",
}

type Storage interface {
	UploadFile(filePath string) (path string, err error)
	DownloadFile(url string) (path string, err error)
}

const FLASH_VERIFY_EMAIL = "verifyMailMessage"

func init()  {
	serializer.For(json.ContentType,json.New())	//注册序列化生成器
}

//将响应结构转换为JSON字符串
func SendWsJsonStr(str *websocketJSON) []byte {
	serializedString, err := serializer.SerializeToString(json.ContentType, str)
	if err != nil {
		fmt.Println("SendWsJsonStr Err : " ,err.Error())
		return []byte("")
	}
	return []byte(serializedString)
}

